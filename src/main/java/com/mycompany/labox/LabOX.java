/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.labox;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class LabOX {

    static char[][] table = {{'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}};
    static char currentPlayer = 'X';
    static int row, col;
    static int count = 0;

    public static void main(String[] args) {
        printWelcome();
        while (true) {

            printTable();
            printTurn();
            inputNumber();
            if (isWin()) {
                printTable();
                printWin();
                break;
            }else if(isDraw()){
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcom To OX game");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputNumber() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please Input Number to Place : ");
            int number = sc.nextInt();
            col = (number - 1) % 3;
            row = (number - 1) / 3;
            if (table[row][col] == 'X' || table[row][col] == 'O') {
                switchPlayer();
                return;
            }
            table[row][col] = currentPlayer;
            count++;
            return;

        }

    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }

    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (X1()) {
            return true;
        } else if (X2()) {
            return true;
        }
        return false;
    
    }
    
    

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != currentPlayer) {
                return false;

            }
        }
        return true;

    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != currentPlayer) {
                return false;

            }
        }
        return true;
    }

    private static boolean X1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    private static boolean X2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][(3 - 1) - i] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    private static void printWin() {
        System.out.println(currentPlayer + " Win !!!");
    
    }

    private static boolean isDraw() {
        if(count != 9){
        } else {
            return true;
        }
    return false;
    }

    private static void printDraw() {
        System.out.println(currentPlayer+" Draw !!!");
    }

    
}
